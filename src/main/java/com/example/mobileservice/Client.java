package com.example.mobileservice;

import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vrg
 */
public final class Client {


    private final static AtomicLong counter = new AtomicLong();
    private final static AtomicLong counter2 = new AtomicLong();

    private final long id = counter.incrementAndGet();

    private MobilePhoneRepairService service;
    private final Mobile mobile;
    private Random random = new Random();
    private static final Manufacturer[] manufacturers = Manufacturer.values();
    private static final String[] samsungModels = {"Galaxy S3", "Galaxy S4", "Galaxy S5", "Galaxy S6", "Galaxy S7", "Galaxy Note"};
    private static final String[] htcModels = {"One", "Desire 610", "One Mini"};
    private static final String[] huaweiModels = {"Ascend P7", "Ascend P2", "Ascend G630"};
    private static final String[] appleModels = {"iPhone 4s", "iPhone 5s", "iPhone5c", "iPhone 6", "iPhone 6s", "iPhone 7", "iPhone 8", "iPhone X"};

    public Client(MobilePhoneRepairService service) {
        this.service = service;
        this.mobile = createRandomPhone();
    }

    private String generateSerial() {
        return String.valueOf(1000000L + counter2.incrementAndGet());
    }

    public Mobile createRandomPhone() {
        Manufacturer m = manufacturers[random.nextInt(manufacturers.length)];
        switch (m) {
            case APPLE:
                return createApplePhone();
            case HTC:
                return createHTCPhone();
            case HUAWEI:
                return createHuaweiPhone();
            case SAMSUNG:
                return createSamsungPhone();
            default:
                throw new AssertionError("Unexpected manufacturer: " + m);
        }
    }

    private Mobile createSamsungPhone() {
        Mobile mobile = new Mobile();
        mobile.manufacturer = Manufacturer.SAMSUNG;
        mobile.model = samsungModels[random.nextInt(samsungModels.length)];
        mobile.display = new MobilePart(new MobilePartType(mobile.manufacturer, MobilePartUnit.DISPLAY, mobile.model + " Display"), random.nextBoolean(), generateSerial());
        mobile.microphone = new MobilePart(new MobilePartType(mobile.manufacturer, MobilePartUnit.MICROPHONE, mobile.model + " Microphone"), random.nextBoolean(), generateSerial());
        mobile.motherBoard = new MobilePart(new MobilePartType(mobile.manufacturer, MobilePartUnit.MOTHERBOARD, mobile.model + " MotherBoard"), random.nextBoolean(), generateSerial());
        mobile.powerSwitch = new MobilePart(new MobilePartType(mobile.manufacturer, MobilePartUnit.POWER_SWITCH, mobile.model + " Power Switch"), random.nextBoolean(), generateSerial());
        mobile.speaker = new MobilePart(new MobilePartType(mobile.manufacturer, MobilePartUnit.SPEAKER, mobile.model + " Speaker"), random.nextBoolean(), generateSerial());
        mobile.microphone = new MobilePart(new MobilePartType(mobile.manufacturer, MobilePartUnit.MICROPHONE, mobile.model + " Microphone"), random.nextBoolean(), generateSerial());
        mobile.volumeButtons = new MobilePart(new MobilePartType(mobile.manufacturer, MobilePartUnit.VOLUME_BUTTONS, mobile.model + " Volume Buttons"), random.nextBoolean(), generateSerial());
        return mobile;
    }

    private Mobile createApplePhone() {
        Mobile mobile = new Mobile();
        mobile.manufacturer = Manufacturer.APPLE;
        mobile.model = appleModels[random.nextInt(appleModels.length)];
        mobile.display = new MobilePart(new MobilePartType(mobile.manufacturer, MobilePartUnit.DISPLAY, mobile.model + " Display"), random.nextBoolean(), generateSerial());
        mobile.microphone = new MobilePart(new MobilePartType(mobile.manufacturer, MobilePartUnit.MICROPHONE, mobile.model + " Microphone"), random.nextBoolean(), generateSerial());
        mobile.motherBoard = new MobilePart(new MobilePartType(mobile.manufacturer, MobilePartUnit.MOTHERBOARD, mobile.model + " MotherBoard"), random.nextBoolean(), generateSerial());
        mobile.powerSwitch = new MobilePart(new MobilePartType(mobile.manufacturer, MobilePartUnit.POWER_SWITCH, mobile.model + " Power Switch"), random.nextBoolean(), generateSerial());
        mobile.speaker = new MobilePart(new MobilePartType(mobile.manufacturer, MobilePartUnit.SPEAKER, mobile.model + " Speaker"), random.nextBoolean(), generateSerial());
        mobile.microphone = new MobilePart(new MobilePartType(mobile.manufacturer, MobilePartUnit.MICROPHONE, mobile.model + " Microphone"), random.nextBoolean(), generateSerial());
        mobile.volumeButtons = new MobilePart(new MobilePartType(mobile.manufacturer, MobilePartUnit.VOLUME_BUTTONS, mobile.model + " Volume Buttons"), random.nextBoolean(), generateSerial());
        return mobile;
    }

    private Mobile createHTCPhone() {
        Mobile mobile = new Mobile();
        mobile.manufacturer = Manufacturer.HTC;
        mobile.model = htcModels[random.nextInt(htcModels.length)];
        mobile.display = new MobilePart(new MobilePartType(mobile.manufacturer, MobilePartUnit.DISPLAY, mobile.model + " Display"), random.nextBoolean(), generateSerial());
        mobile.microphone = new MobilePart(new MobilePartType(mobile.manufacturer, MobilePartUnit.MICROPHONE, mobile.model + " Microphone"), random.nextBoolean(), generateSerial());
        mobile.motherBoard = new MobilePart(new MobilePartType(mobile.manufacturer, MobilePartUnit.MOTHERBOARD, mobile.model + " MotherBoard"), random.nextBoolean(), generateSerial());
        mobile.powerSwitch = new MobilePart(new MobilePartType(mobile.manufacturer, MobilePartUnit.POWER_SWITCH, mobile.model + " Power Switch"), random.nextBoolean(), generateSerial());
        mobile.speaker = new MobilePart(new MobilePartType(mobile.manufacturer, MobilePartUnit.SPEAKER, mobile.model + " Speaker"), random.nextBoolean(), generateSerial());
        mobile.microphone = new MobilePart(new MobilePartType(mobile.manufacturer, MobilePartUnit.MICROPHONE, mobile.model + " Microphone"), random.nextBoolean(), generateSerial());
        mobile.volumeButtons = new MobilePart(new MobilePartType(mobile.manufacturer, MobilePartUnit.VOLUME_BUTTONS, mobile.model + " Volume Buttons"), random.nextBoolean(), generateSerial());
        return mobile;
    }

    private Mobile createHuaweiPhone() {
        Mobile mobile = new Mobile();
        mobile.manufacturer = Manufacturer.HUAWEI;
        mobile.model = huaweiModels[random.nextInt(huaweiModels.length)];
        mobile.display = new MobilePart(new MobilePartType(mobile.manufacturer, MobilePartUnit.DISPLAY, mobile.model + " Display"), random.nextBoolean(), generateSerial());
        mobile.microphone = new MobilePart(new MobilePartType(mobile.manufacturer, MobilePartUnit.MICROPHONE, mobile.model + " Microphone"), random.nextBoolean(), generateSerial());
        mobile.motherBoard = new MobilePart(new MobilePartType(mobile.manufacturer, MobilePartUnit.MOTHERBOARD, mobile.model + " MotherBoard"), random.nextBoolean(), generateSerial());
        mobile.powerSwitch = new MobilePart(new MobilePartType(mobile.manufacturer, MobilePartUnit.POWER_SWITCH, mobile.model + " Power Switch"), random.nextBoolean(), generateSerial());
        mobile.speaker = new MobilePart(new MobilePartType(mobile.manufacturer, MobilePartUnit.SPEAKER, mobile.model + " Speaker"), random.nextBoolean(), generateSerial());
        mobile.microphone = new MobilePart(new MobilePartType(mobile.manufacturer, MobilePartUnit.MICROPHONE, mobile.model + " Microphone"), random.nextBoolean(), generateSerial());
        mobile.volumeButtons = new MobilePart(new MobilePartType(mobile.manufacturer, MobilePartUnit.VOLUME_BUTTONS, mobile.model + " Volume Buttons"), random.nextBoolean(), generateSerial());
        return mobile;
    }

    public void log(String message) {
        System.out.println("Client #" + id + ": " + message);
    }

    public void sendMobileToService() {
        log("Sending in mobile: " + mobile);
        WorkSheet workSheet = service.sendIn(mobile);
        WorkSheet.Status lastKnownStatus = workSheet.status;
        while (lastKnownStatus != WorkSheet.Status.FINISHED) {
            while (workSheet.status == lastKnownStatus) {
                try {
                    Thread.sleep(1000L);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            log("OrderStatus changed to " + workSheet.status);
            lastKnownStatus = workSheet.status;
        }
    }

}
