package com.example.mobileservice.listener;

public interface StatusChangeListener<E extends AbstractStatusChangedEvent> {
    void statusChanged(E statusChangedEvent);
}
