package com.example.mobileservice.listener;

import java.util.EventObject;

public class AbstractStatusChangedEvent<SOURCE,T> extends EventObject {

    private T objectWithChangedStatus;

    public AbstractStatusChangedEvent(SOURCE source,T objectWithChangedStatus) {
        super(source);
        this.objectWithChangedStatus = objectWithChangedStatus;
    }

    @Override
    public SOURCE getSource() {
        return (SOURCE) super.getSource();
    }

    public T getObjectWithChangedStatus() {
        return objectWithChangedStatus;
    }
}
