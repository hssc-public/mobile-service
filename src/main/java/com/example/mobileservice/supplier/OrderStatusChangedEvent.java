package com.example.mobileservice.supplier;

import com.example.mobileservice.listener.AbstractStatusChangedEvent;
import com.example.mobileservice.supplier.impl.InternalOrder;

public class OrderStatusChangedEvent extends AbstractStatusChangedEvent<Order,Order> {

    public OrderStatusChangedEvent(InternalOrder mobilePartSupplier, Order order) {
        super(mobilePartSupplier, order);
    }
}
