package com.example.mobileservice;

/**
 *
 * @author vrg
 */
public class ReplacedMobilePart extends MobilePart {

    public ReplacedMobilePart(MobilePartType type, String serialNumber) {
        super(type, false, serialNumber);
    }
    
}
