package com.example.mobileservice.helper;

public interface SerialNumberGenerator {
    String generateSerialNumber();
}
