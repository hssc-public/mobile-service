package com.example.mobileservice.helper;

public interface IdGenerator {
    long generateId();
}
