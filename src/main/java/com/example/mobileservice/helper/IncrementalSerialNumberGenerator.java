package com.example.mobileservice.helper;

import java.util.concurrent.atomic.AtomicLong;

public class IncrementalSerialNumberGenerator implements SerialNumberGenerator {
    private AtomicLong counter = new AtomicLong(20000000L);


    @Override
    public String generateSerialNumber() {
        return String.valueOf(counter.incrementAndGet());
    }
}
