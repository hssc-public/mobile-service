package com.example.mobileservice.helper;

import java.util.concurrent.atomic.AtomicLong;

public class IncrementalIdGenerator implements IdGenerator {
    private AtomicLong counter = new AtomicLong();


    @Override
    public long generateId() {
        return counter.incrementAndGet();
    }
}
