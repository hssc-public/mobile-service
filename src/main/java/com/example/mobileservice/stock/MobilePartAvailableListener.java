package com.example.mobileservice.stock;

import com.example.mobileservice.MobilePart;

public interface MobilePartAvailableListener {
    void mobilePartIsAvailable(MobilePart mobilePart);
}
