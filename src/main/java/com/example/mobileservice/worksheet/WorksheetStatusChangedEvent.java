package com.example.mobileservice.worksheet;

import com.example.mobileservice.MobilePhoneRepairService;
import com.example.mobileservice.WorkSheet;
import com.example.mobileservice.listener.AbstractStatusChangedEvent;

public class WorksheetStatusChangedEvent extends AbstractStatusChangedEvent<MobilePhoneRepairService,WorkSheet> {

    public WorksheetStatusChangedEvent(MobilePhoneRepairService mobilePartSupplier, WorkSheet workSheet) {
        super(mobilePartSupplier, workSheet);
    }
}
