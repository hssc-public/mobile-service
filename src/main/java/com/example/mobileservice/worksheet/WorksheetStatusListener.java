package com.example.mobileservice.worksheet;

public interface WorksheetStatusListener {
    void worksheetStatusChanged(WorksheetStatusChangedEvent worksheetStatusChangedEvent);
}
